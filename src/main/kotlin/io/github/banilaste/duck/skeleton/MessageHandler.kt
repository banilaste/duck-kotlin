package io.github.banilaste.duck.skeleton

import java.nio.ByteBuffer

/**
 * Handler for a message type
 *
 * @see MessageType for implementations
 */
interface MessageHandler {
    fun handle(buffer: ByteBuffer, callId: Int, localCallId: String): MessageAnswer?
}