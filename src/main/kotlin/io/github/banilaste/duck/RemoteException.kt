package io.github.banilaste.duck


class RemoteException(exceptionClass: String?, message: String?):
    Exception("$exceptionClass: $message")
