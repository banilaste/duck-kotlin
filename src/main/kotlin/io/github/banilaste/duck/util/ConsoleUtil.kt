package io.github.banilaste.duck.util


/**
 * Parse main() arguments into a map. The format for argument is --key=value, or --key, other arguments are ignored
 */
fun parseArgs(args: Array<String>): Map<String, String> {
    val enabling = "--([a-zA-Z]+)".toRegex()
    val affectation = "--([a-zA-Z]+)=([a-zA-Z0-9:._\\-/\\\\]+)".toRegex()

    val parsed = HashMap<String, String>()

    args.forEach {
        // Parse every argument into the map, first affectation
        affectation.matchEntire(it)?.destructured?.let { (name, value) -> parsed[name] = value }
            // Or just enabling of a key
            ?: enabling.matchEntire(it)?.destructured?.let { (name) -> parsed[name] = "" }
    }

    return parsed
}

/**
 * Retrieve an argument from argument map or ask the user for it
 */
fun <T> getOrInput(name: String, args: Map<String, String>, readableName: String, convertOrReject: (String) -> T?): T {
    // Try to retrieve from arguments
    var result = args[name]?.let { convertOrReject(it) }

    while (result == null) {
        LOGGER.warn("no valid $readableName provided")
        print("$name > ")
        result = readLine()?.let { convertOrReject(it) }
    }

    return result
}