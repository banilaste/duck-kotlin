package io.github.banilaste.duck.util

enum class LogLevel(val value: Int, val color: String? = null) {
    VERBOSE(50, "\u001B[35m"),
    DEBUG(100, "\u001B[35m"),
    WARNING(400, "\u001B[33m"),
    INFO(500),
    ERROR(800, "\u001B[31m"),
    RESULT(1000, "\u001B[36m")
}

/**
 * Simple logger for DUCK purpose
 */
object LOGGER {
    var level = LogLevel.WARNING
    var colorsEnabled = true

    private fun log(level: LogLevel, content: Any?) {
        if (level.value >= this.level.value) {
            level.color?.takeIf { colorsEnabled }?.also { print(it) }
            println("$level: $content\u001B[0m")
        }
    }

    fun info(string: Any?) = log(
        LogLevel.INFO,
        string
    )
    fun error(string: Any?) = log(
        LogLevel.ERROR,
        string
    )
    fun debug(string: Any?) = log(
        LogLevel.DEBUG,
        string
    )
    fun warn(string: Any?) = log(
        LogLevel.WARNING,
        string
    )
    fun result(string: Any?) = log(
        LogLevel.RESULT,
        string
    )
    fun verbose(string: Any?) = log(
        LogLevel.VERBOSE,
        string
    )
}