package io.github.banilaste.duck.proxy

import io.github.banilaste.duck.RemoteException
import io.github.banilaste.duck.RemoteObject
import io.github.banilaste.duck.network.DatagramHandler
import io.github.banilaste.duck.util.LOGGER
import java.nio.ByteBuffer
import kotlin.math.E
import kotlin.math.ln

/**
 * Thread performing request repetition when the promise is not complete after some amount of time
 */
class RequestRepeater(
    private val callId: Int,
    private val remoteObject: RemoteObject,
    private val messageBuffer: ByteBuffer,
    private val promise: Promise
) : Thread() {
    init {
        this.start()
    }

    override fun run() {
        for (i in 1..4) {
            // Wait with logarithmically increasing time (500ms, 1693ms, 3147ms)
            sleep(500L * i * ln(E * i.toDouble()).toLong())

            // Check if still waiting for answer
            promise.ifNotComplete {
                if (i < 4) {
                    // Try again
                    LOGGER.debug("request $callId was not answered yet, sending request again (retry $i)")
                    DatagramHandler.send(
                        callId,
                        messageBuffer.array(),
                        remoteObject.identifier.ip,
                        remoteObject.identifier.port,
                        i // attempt number
                    )
                } else {
                    // Final timeout
                    LOGGER.debug("request $callId was not answered yet, giving up")
                    promise.error = RemoteException(
                        "RemoteException",
                        "unable to fulfill request, the server is not answering"
                    )
                    promise.condition.signal()
                }
            }
        }
    }
}