package io.github.banilaste.duck.proxy

import io.github.banilaste.duck.RemoteException
import io.github.banilaste.duck.RemoteObject
import io.github.banilaste.duck.marshall.marshall
import io.github.banilaste.duck.marshall.typeOf
import io.github.banilaste.duck.marshall.unmarshall
import io.github.banilaste.duck.network.DatagramHandler
import io.github.banilaste.duck.skeleton.MessageType
import java.nio.ByteBuffer
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.reflect.KType

/**
 * Object allowing to call remote procedures on remote objects. For each of its function,
 * you should provide a remote object, a method or property id, and the arguments.
 *
 * The thread is locked until the answer is received, in case of failure (network or remote), a
 * remote exception is thrown.
 */
object Proxy {
    private val locks = HashMap<Int, Promise>()

    /**
     * Call the setter of a remote object property
     */
    fun <T : Any?> set(obj: RemoteObject, propertyId: Int, value: T) {
        val content = marshall(value)

        syncCall(
            buildMessage(
                obj,
                MessageType.SET,
                ByteBuffer.allocate(4 + content.size).putInt(propertyId).put(content)
            ),
            obj
        )?.also {
            if (!unmarshall<Boolean>(it)) {
                throw Error("unable to call setter")
            }
        }
    }

    /**
     * Calls the getter of a remote object property
     */
    fun <T : Any> get(obj: RemoteObject, propertyId: Int, expectedReturn: KType): T {
        return syncCall(
            buildMessage(
                obj,
                MessageType.GET,
                ByteBuffer.allocate(4).putInt(propertyId)
            ),
            obj
        )?.let { unmarshall<T>(it, expectedReturn) }!!
    }

    /**
     * Call a method of the remote object, and return the result of the given type
     */
    fun <T : Any> call(obj: RemoteObject, methodId: Int, args: Array<Any>, expectedReturn: KType): T? {
        val arguments = args.map { marshall(it) }
        val size = arguments.fold(0) { acc, bytes -> acc + bytes.size }

        val buffer = ByteBuffer.allocate(size + 4)
            .putInt(methodId)

        arguments.forEach { buffer.put(it) }

        return syncCall(
            buildMessage(
                obj,
                MessageType.CALL,
                buffer
            ),
            obj
        )?.let { unmarshall(it, expectedReturn) }
    }

    /**
     * Build a message to send to the remote server, containing the remote object identifier, the message type code,
     * and the data content (arguments)
     */
    private fun buildMessage(remoteObject: RemoteObject, type: MessageType, dataContent: ByteBuffer): ByteBuffer {
        val remoteObjectBytes = marshall(remoteObject)
        val content = dataContent.array()

        return ByteBuffer.allocate(1 + remoteObjectBytes.size + content.size)
            .put(type.code)
            .put(remoteObjectBytes)
            .put(content)
    }

    /**
     * Call asynchronously a remote call procedure (either get, set or function), returns the byte buffer obtained with
     * the result
     */
    private fun syncCall(buffer: ByteBuffer, remoteObject: RemoteObject): ByteBuffer? {
        val callId = DatagramHandler.send(buffer.array(), remoteObject.identifier.ip, remoteObject.identifier.port)

        // Lock thread and wait for result
        val lock = ReentrantLock()
        val condition = lock.newCondition()
        val promise = Promise(lock, condition)
        locks[callId] = promise

        // Launch timeout process, to re-send data in case the server does not answer
        RequestRepeater(callId, remoteObject, buffer, promise)

        // Lock current thread to wait
        lock.withLock {
            condition.await()
        }

        // Throw error if error specified
        promise.error?.let {
            throw it
        }

        return promise.result
    }

    /**
     * Resume a thread locked by a remote method invocation when the result is here or an error is thrown
     */
    fun unlock(callId: Int, result: ByteBuffer?, error: Boolean = false) {
        locks[callId]?.let {
            it.lock.withLock {
                if (!error) {
                    it.result = result
                } else {
                    it.error = RemoteException(
                        unmarshall(result!!, typeOf(String::class, 0, true)),
                        unmarshall(result, typeOf(String::class, 0, true))
                    )
                }
                it.condition.signal()
            }
        }
    }
}

