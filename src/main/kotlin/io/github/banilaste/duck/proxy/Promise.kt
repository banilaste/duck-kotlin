package io.github.banilaste.duck.proxy

import io.github.banilaste.duck.RemoteException
import java.nio.ByteBuffer
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Storage of a request and it's lock.
 */
data class Promise (
    val lock: ReentrantLock,
    val condition: Condition,
    var result: ByteBuffer? = null,
    var error: RemoteException? = null
) {
    /**
     * Check if the request is not complete, if so, run the provided function
     * with the promise lock locked
     */
    fun ifNotComplete(fn: () -> Unit) {
        lock.withLock {
            if (result == null && error == null) {
                fn()
            }
        }
    }
}