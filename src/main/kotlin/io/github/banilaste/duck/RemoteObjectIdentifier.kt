package io.github.banilaste.duck

import java.net.InetAddress
import java.util.*

/**
 * Unique identifier for a remote object
 */
data class RemoteObjectIdentifier(val code: Int, val ip: InetAddress, val port: Int) {
    override fun toString(): String {
        return "{ code=$code, ip=$ip, port=$port }"
    }
}