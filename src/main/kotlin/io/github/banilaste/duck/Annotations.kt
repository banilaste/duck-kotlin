package io.github.banilaste.duck

/**
 * Member of a remote interface (field or function)
 */
annotation class RemoteMember(val memberId: Int)

/**
 * Remote interface (abstract definition of a remote object)
 */
annotation class RemoteInterface

/**
 * Stub for a remote object
 */
annotation class RemoteStub

/**
 * Mark a method which request should be repeated and result not saved
 */
annotation class NoCaching
