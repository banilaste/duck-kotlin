package io.github.banilaste.duck

import java.net.DatagramSocket
import java.net.InetAddress

/**
 * Properties of the datagram server
 */
object DuckProperties {
    var localAddress: InetAddress = InetAddress.getLocalHost()
    var localPort = 22333
    var socketFactory: (port: Int) -> DatagramSocket = { DatagramSocket(it) }
}
