package io.github.banilaste.duck.network

import java.nio.ByteBuffer

data class Request(val data: ArrayList<ByteArray?> = ArrayList(), var last: Int = -1, var triggered: Long = -1) {
    /**
     * Check if all the data is received
     */
    fun done(): Boolean {
        if (last == -1 || data.size - 1 < last) return false

        repeat(last) {
            if (data[it] == null)
                return false
        }

        return true
    }

    /**
     * Regroup all the data into a single buffer
     */
    fun compile(): ByteBuffer {
        return ByteBuffer.allocate(data.fold(0) { acc, bytes -> acc + bytes!!.size })
            .also { buffer ->
                data.forEach { part ->
                    buffer.put(part)
                }
            }
            .also {
                it.position(0)
            }
    }

    fun put(packetNumber: Int, packet: ByteArray, last: Boolean) {
        while (packetNumber >= data.size) {
            data.add(null)
        }

        data[packetNumber] = packet
        if (last) {
            this.last = packetNumber
        }
    }
}