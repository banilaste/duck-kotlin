package io.github.banilaste.duck.marshall

import io.github.banilaste.duck.RemoteObject
import io.github.banilaste.duck.RemoteObjectIdentifier
import java.net.InetAddress
import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.KTypeProjection
import kotlin.reflect.full.*
import kotlin.reflect.jvm.javaConstructor

class MarshallingException(message: String) : Exception(message)

const val NULL = (-128).toByte()
const val ESCAPE = (-127).toByte()

fun marshall(obj: Any?, escape: Boolean = true): ByteArray {
    if (obj == null) {
        return ByteArray(1) { NULL }
    }

    return when (obj) {
        is List<*> -> {
            val objects = obj.map { marshall(it) }
            ByteBuffer.allocate(4 + objects.fold(0, { acc, value -> value.size + acc })).also { buffer ->
                buffer.putInt(objects.size)
                objects.forEach { part -> buffer.put(part) }
            }
        }
        is Int -> ByteBuffer.allocate(4).putInt(obj)
        is Long -> ByteBuffer.allocate(8).putLong(obj)
        is Float -> ByteBuffer.allocate(4).putFloat(obj)
        is Double -> ByteBuffer.allocate(8).putDouble(obj)
        is Boolean -> ByteBuffer.allocate(1).put((if (obj) 1 else 0).toByte())
        is ByteArray -> ByteBuffer.allocate(obj.size + 4).putInt(obj.size).put(obj)
        is String -> {
            val bytes = obj.toByteArray(StandardCharsets.UTF_8)
            ByteBuffer.allocate(4 + bytes.size).putInt(bytes.size).put(bytes)
        }
        is InetAddress -> ByteBuffer.allocate(obj.address.size + 4).putInt(obj.address.size).put(obj.address)
        is RemoteObject -> {
            val ip = marshall(obj.identifier.ip)
            ByteBuffer.allocate(8 + ip.size)
                .putInt(obj.identifier.code)
                .put(ip)
                .putInt(obj.identifier.port)

        }
        is Unit -> ByteBuffer.allocate(0)
        else -> {
            if (obj::class.isData) {
                // Find all arguments of primary constructor
                val contents = obj::class.primaryConstructor?.let { constructor ->
                    constructor.parameters.mapIndexed { index, param ->
                        marshall(
                            // Expect all constructor args to be var or val
                            obj::class.declaredMemberProperties.find { it.name == param.name }!!.getter.call(obj),
                            index > 0
                        )
                    }
                }!!

                // Combine all
                ByteBuffer.allocate(contents.fold(0) { acc, it -> acc + it.size }).also { buffer ->
                    contents.forEach {
                        buffer.put(it)
                    }
                }
            } else {
                throw MarshallingException("unsupported type : ${obj::class.simpleName}")
            }
        }
    }
        .array()
        .let {
            // Escape first byte same value as if null or escape
            if (it.isNotEmpty() && escape && (it[0] == NULL || it[0] == ESCAPE)) {
                ByteBuffer.allocate(it.size + 1)
                    .put(ESCAPE)
                    .put(it)
                    .array()
            } else {
                it
            }
        }
}

inline fun <reified T: Any> unmarshall(buffer: ByteBuffer): T {
    return unmarshall(buffer, T::class.createType())
}

@Suppress("IMPLICIT_CAST_TO_ANY")
fun <T: Any?> unmarshall(buffer: ByteBuffer, kType: KType, notEscaped: Boolean = true): T {
    // Handle null
    if (notEscaped && kType.classifier != Unit::class && buffer[buffer.position()] == NULL) {
        buffer.get()

        if (!kType.isMarkedNullable) {
            throw MarshallingException("null received for non null type")
        }

        return null as T
    }

    // Skip escape character
    if (notEscaped && kType.classifier != Unit::class && buffer[buffer.position()] == ESCAPE.toByte()) {
        buffer.get()
    }

    @Suppress("UNCHECKED_CAST")
    return when (kType.classifier) {
        List::class -> {
            List<T>(buffer.int) {
                unmarshall(buffer, kType.arguments[0].type!!)
            }
        }
        Int::class -> buffer.int
        Long::class -> buffer.long
        Float::class -> buffer.float
        Double::class -> buffer.double
        Boolean::class -> buffer.get() != (0).toByte()
        String::class -> {
            val bytes = ByteArray(buffer.int)
            buffer.get(bytes)
            String(bytes, StandardCharsets.UTF_8)
        }
        ByteArray::class -> {
            val bytes = ByteArray(buffer.int)
            buffer.get(bytes)
            bytes
        }
        InetAddress::class -> {
            val size = buffer.int
            val result = ByteArray(size)
            buffer.get(result)
            InetAddress.getByAddress(result)
        }
        RemoteObjectIdentifier::class -> RemoteObjectIdentifier(
            buffer.int,
            unmarshall(buffer, typeOf(InetAddress::class)),
            buffer.int
        )
        Unit::class -> 0
        else -> {
            val clazz = kType.classifier as KClass<*>

            when {
                clazz.isSubclassOf(RemoteObject::class) -> RemoteObject.stub(
                        unmarshall(buffer, RemoteObjectIdentifier::class.createType(), false),
                        clazz as KClass<out RemoteObject>
                    )
                clazz.isData -> clazz.primaryConstructor?.let { constructor ->
                        // Build object
                        val args = constructor.parameters.mapIndexed { index, it ->
                            unmarshall<Any>(buffer, it.type, index > 0)
                        }.toTypedArray()
                        constructor.javaConstructor?.newInstance(*args)
                    }
                else -> throw MarshallingException("unsupported type : ${ clazz.simpleName }")
            }
        }
    } as T
}

/**
 * Create kotlin type for given non generic type, either array with give depth or the class type
 */
fun <T : Any> typeOf(clazz: KClass<T>, depth: Int = 0, nullable: Boolean = false): KType {
    if (depth <= 0) {
        return clazz.createType().withNullability(nullable)
    }

    return List::class.createType(listOf(
        KTypeProjection.invariant(
            typeOf(clazz, depth - 1, nullable)
        )
    ), nullable = nullable)
}