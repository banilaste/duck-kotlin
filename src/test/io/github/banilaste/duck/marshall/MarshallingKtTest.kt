package io.github.banilaste.duck.marshall

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.nio.ByteBuffer
import kotlin.reflect.KType

data class NestedObject(val number: Int, val string: String, val children: NestedObject?)

internal class MarshallingKtTest {

    private fun assertMarshall(obj: Any, type: KType) {
        assertEquals(obj, unmarshall(ByteBuffer.wrap(marshall(obj)), type))
    }

    @Test
    fun testMarshallingList() {
        val arrays = listOf(
            listOf(3, 45, 3),
            listOf(3.89, 0.0, -156.2),
            listOf("hello", "bonjour", null, "string"),
            listOf(
                listOf(
                    NestedObject(-1, "oui", null),
                    null,
                    NestedObject(2, "oui", NestedObject(2, "oui", null))
                ),
                null,
                listOf(
                    NestedObject(278, "non", null)
                )
            )
        )
        val types = listOf(
            typeOf(Int::class, 1),
            typeOf(Double::class, 1),
            typeOf(String::class, 1, true),
            typeOf(NestedObject::class, 2, true)
        )

        arrays.zip(types).forEach { (array, type) ->
            assertMarshall(array, type)
        }
    }

    @Test
    fun testEscaping() {
        val arrays = listOf(
            -1,
            -2,
            NestedObject(-1, "oui", null),
            NestedObject(-2, "oui", NestedObject(-1, "oui", null)),
            listOf(-1, 255, null, -2)
        )
        val types = listOf(
            typeOf(Int::class),
            typeOf(Int::class),
            typeOf(NestedObject::class),
            typeOf(NestedObject::class),
            typeOf(Int::class, 1, true)
        )

        arrays.zip(types).forEach { (array, type) ->
            assertMarshall(array, type)
        }
    }
}
