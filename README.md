# Duck networking library for kotlin

This is the kotlin implementation of the duck (Declarative Ultimate Correspondance Kit) network framework. This library is intended to be used along with the [source generator](https://gitlab.com/banilaste/duck) but it is not mandatory.

## Include to the project

Maven is used for inluding most dependencies of this project, you can include the project by adding this project repository and the correct dependency :
```xml
<project>
    <!-- ... -->
    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/16881175/packages/maven</url>
        </repository>
        <!-- ... -->
    </repositories>
    <!-- ... -->
    <dependencies>
        <!-- Network library -->
        <dependency>
            <artifactId>duck-kotlin</artifactId>
            <groupId>io.github.banilaste.duck</groupId>
            <version>1.0-SNAPSHOT</version>
        </dependency>

        <!-- ... -->
    </dependencies>
    <!-- ... -->
</project>
```

## Running a project

Once you have included the code generator, generated the interface and added the interfaces into your project source directories, you can start using the interfaces.

The two main methods you should know in order to have a working project, is how to start the server and register a remote object. Here a the different files used for a very simple remote object.

### Interface file :
```
🦆 MyRemoteObject
	💧 send
        string content
```

### Client side
```kotlin
import io.github.banilaste.duck.generated.DuckProject
import io.github.banilaste.duck.generated.MyRemoteObject
import io.github.banilaste.duck.RemoteObject
import io.github.banilaste.duck.RemoteObjectIdentifier

fun main() {
    // Start local server
    DuckProject.init(11111)
    
    // Stub the remote object
    val remoteObject = RemoteObject.stub(
        // Address of the remote object (here we assume that it's id is 0)
        RemoteObjectIdentifier(0, InetAddress.getByName("localhost"), 22222),
        
        // Type of the remote object, a MyRemoteObjectStub will be created
        MyRemoteObject::class
    )

    // The server program should output the message :)
    remoteObject.send("Hello world !")
}
```

### Server side
```kotlin
import io.github.banilaste.duck.generated.MyRemoteObject
import io.github.banilaste.duck.generated.DuckProject

// Client side
fun main() {
    // Start local server
    DuckProject.init(22222)

    // Create implementation
    val remoteImpl = MyRemoteObjectImpl()
}

class MyRemoteObjectImpl: MyRemoteObject() { // the super constructor assign an id to this remote object
    override fun send(content: String) {
        println("received: $content")
    }
}

```
